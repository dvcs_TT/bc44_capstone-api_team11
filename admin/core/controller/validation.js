var showMessage = function (status, notiId, message) {
  if (status == false) {
    getEle(notiId).style.display = "block";
    getEle(notiId).innerHTML = `${message}`;
    return false;
  } else if (status == true) {
    getEle(notiId).style.display = "none";
    return true;
  }
};

var isNotEmpty = function (id, notiId) {
  var inputVal = getEle(id).value;

  return inputVal === ""
    ? showMessage(false, notiId, `(*)This field is required`)
    : showMessage(true, notiId);
};

var isSelected = function (id, notiId) {
  var selectTag = getEle(id);
  return selectTag.selectedIndex == 0
    ? showMessage(false, notiId, `(*)Please select one option`)
    : showMessage(true, notiId);
};

var isNumber = function (id, notiId) {
  var numRegex = /^[0-9]+$/;
  var inputVal = getEle(id).value;
  return !inputVal.match(numRegex)
    ? showMessage(
        false,
        notiId,
        `(*)This field is required and must be a number`
      )
    : showMessage(true, notiId);
};

var isNotExisted = function (isUpdate) {
  var tBody = getEle("tableList");
  var rows = Array.from(tBody.querySelectorAll("tr"));
  var nameColIndex = 2;
  var tbNameArr = [];
  var prodNameInput = getEle("name");

  rows.forEach((row) => {
    var nameColText = row.querySelector(
      `td:nth-child(${nameColIndex})`
    ).textContent;
    tbNameArr.push(nameColText);
  });

  console.log(isUpdate);
  if (isUpdate) return showMessage(true, "nameNoti");
  var result = tbNameArr.filter((tbName) => {
    return (
      tbName.toLowerCase().replaceAll(" ", "") ==
      prodNameInput.value.toLowerCase().replaceAll(" ", "")
    );
  });
  return result.length > 0
    ? showMessage(false, "nameNoti", `(*)Product is existed`)
    : showMessage(true, "nameNoti");
};

var validateForm = function (isUpdate) {
  var valid = true;
  valid &= isNotEmpty("name", "nameNoti") && isNotExisted(isUpdate);
  valid &= isNumber("price", "priceNoti");
  valid &= isNotEmpty("screen", "screenNoti");
  valid &= isNotEmpty("backCamera", "backCamNoti");
  valid &= isNotEmpty("frontCamera", "frontCamNoti");
  valid &= isNotEmpty("img", "imgNoti");
  valid &= isNotEmpty("desc", "descNoti");
  valid &= isSelected("type", "typeNoti");

  return valid;
};
