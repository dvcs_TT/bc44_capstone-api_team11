var BASE_URL = "https://644a88f379279846dceb717e.mockapi.io/product";

var Service = {
  get: function () {
    return axios({
      url: `${BASE_URL}`,
      method: "GET",
    });
  },

  getProdById: function (id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },

  delete: function (id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },

  add: function (data) {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: data,
    });
  },

  update: function (id, data) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
      data: data,
    });
  },
};
