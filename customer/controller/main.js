const BE_BASE_URL = "https://644a88f379279846dceb717e.mockapi.io/product";
const FE_BASE_URL = "https://643a58ba90cd4ba563f77326.mockapi.io/cart";

// Fetch the phone list from back-end & render the cart
function fetchPhoneList() {
  phoneService.getPhones().then(function (res) {
    renderList(res.data.reverse());
  });

  fetchCart();
}
fetchPhoneList();

function fetchCart() {
  cartService.getList().then(function (res) {
    renderCart(res.data.reverse());
  });
}

// Filter phones out by brands
function brandFilter() {
  phoneService.getPhones().then(function (res) {
    var phones = res.data;

    var selectedValue = getEle("selectList").value;

    var filterData = [];
    if (selectedValue == "all") {
      filterData = phones;
    } else {
      filterData = phones.filter(function (phone) {
        return phone.type == selectedValue;
      });
    }

    renderList(filterData);
  });
}

// Add item(s) to the cart
function btnAddToCart(productId) {
  phoneService.getPhoneById(productId).then(function (res) {
    var phoneData = res.data;

    var { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
      phoneData;
    var product = new Product(
      id,
      name,
      price,
      screen,
      backCamera,
      frontCamera,
      img,
      desc,
      type
    );
    var newCartItem = new CartItem(undefined, product, 1);
    findItemById(newCartItem.product.id).then(function (cartItem) {
      if (!cartItem) {
        cartService.create(newCartItem).then(function (res) {
          fetchCart();
        });
      } else {
        cartItem.quantity++;
        cartService.update(cartItem.id, cartItem).then(function (res) {
          fetchCart();
        });
      }
    });
  });
  
  Toastify({
    text: "A new item is added to the cart",
    offset: {
      x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
      y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
    },
  }).showToast();

}

// increase the quantity of an item in the cart
function btnAdd(id) {
  findItemById(id).then(function (cartItem) {
    if (cartItem) {
      cartItem.quantity++;
      cartService.update(cartItem.id, cartItem).then(function (res) {
        fetchCart();
      });
    }
  });
}

// decrease the quantity of an item in the cart
function btnMinus(id) {
  findItemById(id).then(function (cartItem) {
    if (cartItem) {
      cartItem.quantity--;

      if (cartItem.quantity > 0) {
        cartService.update(cartItem.id, cartItem).then(function (res) {
          fetchCart();
        });
      } else {
        cartService.remove(cartItem.id).then(function (res) {
          fetchCart();
        });
      }
    }
  });
}

// Remove item(s) from the cart
function btnRemove(id) {
  cartService.remove(id).then(function (res) {
    fetchCart();
  });
}

// Empty the cart
function emptyCart() {
  cartService
    .getList()
    .then(function (res) {
      var cart = res.data;

      cart.forEach(function (cartItem) {
        cartService.remove(cartItem.id);
      });
    })
    .then(function () {
      setTimeout(() => {
        fetchCart();
      }, 1000);
    });
}

// Pay for item(s) in the cart
function payNow() {
  cartService.getList().then(function (res) {
    var cart = res.data;

    if (cart.length > 0) {
      Swal.fire({
        // position: 'top-end',
        icon: "success",
        title: "Your order is completed",
        showConfirmButton: false,
        timer: 1500,
      });
      emptyCart();
    } else {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Your cart is empty",
      });
    }
  });
}
