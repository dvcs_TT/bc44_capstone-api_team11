var cartService = {
  getList: function () {
    return axios({
      url: `https://643a58ba90cd4ba563f77326.mockapi.io/cart`,
      method: "GET",
    });
  },

  remove: function (id) {
    return axios({
      url: `https://643a58ba90cd4ba563f77326.mockapi.io/cart/${id}`,
      method: "DELETE",
    });
  },

  create: function (data) {
    return axios({
      url: `https://643a58ba90cd4ba563f77326.mockapi.io/cart`,
      method: "POST",
      data: data,
    });
  },

  update: function (id, cartItem) {
    return axios({
      url: `https://643a58ba90cd4ba563f77326.mockapi.io/cart/${id}`,
      method: "PUT",
      data: cartItem,
    });
  },
};
